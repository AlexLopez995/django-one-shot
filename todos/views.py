from django.shortcuts import render, get_object_or_404, redirect
from todos.models import TodoList, TodoItem
from todos.forms import TodoListForm, TodoItemForm


def todo_list(request):
    todo_list = TodoList.objects.all()
    context = {
        "todolist": todo_list,
    }
    return render(request, "todos/list.html", context)


def todo_list_detail(request, id):
    todoslist = get_object_or_404(TodoList, id=id)
    context = {
        "todolist_object": todoslist,
    }
    return render(request, "todos/detail.html", context)


def todo_list_create(request):
    if request.method == "POST":
        form = TodoListForm(request.POST)
        if form.is_valid():
            task = form.save()
            task.save()
            return redirect("todo_list_detail", task.id)
    else:
        form = TodoListForm()
    context = {
        "form": form,
    }
    return render(request, "todos/create.html", context)


def todo_list_update(request, id):
    todo_update = TodoList.objects.get(id=id)
    if request.method == "POST":
        form = TodoListForm(request.POST, instance=todo_update)
        if form.is_valid():
            todo_update = form.save()
            return redirect("todo_list_detail", id=todo_update.id)
    else:
        form = TodoListForm(instance=todo_update)
    context = {
        "form": form,
        "todo_update": todo_update,
    }
    return render(request, "todos/update.html", context)


def todo_list_delete(request, id):
    todo_delete = TodoList.objects.get(id=id)
    if request.method == "POST":
        todo_delete.delete()
        return redirect("todo_list_list")
    return render(request, "todos/delete.html")


def todo_item_create(request):
    if request.method == "POST":
        form = TodoItemForm(request.POST)
        if form.is_valid():
            task = form.save()
            task.save()
            return redirect("todo_list_detail", id=task.list.id)
    else:
        form = TodoItemForm()
    context = {
        "form": form
    }
    return render(request, "todos/item.html", context)


def todo_item_update(request, id):
    todo_task = TodoItem.objects.get(id=id)
    if request.method == "POST":
        form = TodoItemForm(request.POST, instance=todo_task)
        if form.is_valid():
            todo_task = form.save()
            todo_task.save()
            return redirect("todo_list_detail", id=todo_task.id)
    else:
        form = TodoItemForm(instance=todo_task)
    context = {
        "form": form
    }
    return render(request, "todos/ending.html", context)
